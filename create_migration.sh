DATE_TIME=$(date '+%d%m%Y%H%M')
AUTHOR=$(git config user.name)
LIQUIBASE_DIRECTORY_NAME=src/main/resources/liquibase/v.$1
MIGRATION_NAME=v.$1/$DATE_TIME"-"$2.yml

mkdir -p $LIQUIBASE_DIRECTORY_NAME

sed -e 's/AUTHOR/'$AUTHOR'/g; s/DATE_TIME/'$DATE_TIME'/g' template.yml > src/main/resources/liquibase/$MIGRATION_NAME

echo "  - include:
            file: \"$MIGRATION_NAME\"
            relativeToChangelogFile: true" >> src/main/resources/liquibase/master-changelog.yml
