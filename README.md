# LiquibaseMigrationGenerator



## Using
```
./create_migration.sh [version] [migrationName]
```
Params:

- version - Migrations version, for semantic separation
- migrationName - Name of the migration

Examples
```
./create_migration.sh v.0.1.0 add-users-table
```